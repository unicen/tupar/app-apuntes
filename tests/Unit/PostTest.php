<?php

namespace Tests\Unit;

use App\User;
use App\University;
use App\Faculty;
use App\Subject;
use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIsRecent()
    {
        $user = factory(User::class)->create();

        $u = University::create(['name' => 'UNCPBA']);

        $f = Faculty::create([
            'name' => 'EXACTAS',
            'university_id' => $u->id,
        ]);

        $subject = Subject::create([
            'name' => 'Programación Web 2',
            'faculty_id' => $f->id,
        ]);

        $post = factory(Post::class)->create([
            'subject_id' => $subject->id,
            'user_id' => $user->id
        ]);

        $this->assertTrue($post->isRecent);
    }
}
