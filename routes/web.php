<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()) {
        return redirect('home');
    }
    return view('landing');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    /* Resources */
    Route::resource('faculties', 'FacultyController');
    Route::resource('posts', 'PostController')->except(['index','show']);
    Route::resource('universities', 'UniversityController');
    Route::resource('subjects', 'SubjectController');

    /* Custom */
    Route::get('profile', 'ProfileController@show')->name('profile.show');
    Route::post('profile', 'ProfileController@update')->name('profile.update');

    // TODO: Refactor sync tool to enable
    // Route::get('sync/{endpoint}', 'EEController@import');
});

// Public accessible routes
// IMPORTANT: These must be below create, edit routes

Route::resource('posts', 'PostController')->only(['index', 'show']);


Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');
