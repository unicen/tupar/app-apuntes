<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;


class EEImportService {

    private $baseApi = '';

    /**
     * EEImportService constructor.
     * @param string $baseApi
     */
    public function __construct(string $baseApi)
    {
        $this->baseApi = $baseApi;
    }


    public function get($endpoint)
    {
        $response = Http::get("{$this->baseApi}/{$endpoint}");
        return $response;
    }

}
