<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the university.
     *
     * @param  \App\User  $user
     * @param  \App\University  $university
     * @return mixed
     */
    public function update(User $user, User $updatedUser)
    {
        return strtolower($user->role) == 'admin' || $user->id == $updatedUser->id;
    }
}
