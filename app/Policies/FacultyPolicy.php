<?php

namespace App\Policies;

use App\User;
use App\Faculty;
use Illuminate\Auth\Access\HandlesAuthorization;

class FacultyPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any universities.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return strtolower($user->role) == 'admin';
    }

    /**
     * Determine whether the user can view the university.
     *
     * @param  \App\User  $user
     * @param  \App\Faculty $faculty
     * @return mixed
     */
    public function view(User $user, Faculty $faculty)
    {
        return strtolower($user->role) == 'admin';
    }

    /**
     * Determine whether the user can create universities.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return strtolower($user->role) == 'admin';
    }

    /**
     * Determine whether the user can update the university.
     *
     * @param  \App\User  $user
     * @param  \App\Faculty $faculty
     * @return mixed
     */
    public function update(User $user, Faculty $faculty)
    {
        return strtolower($user->role) == 'admin';
    }

    /**
     * Determine whether the user can delete the university.
     *
     * @param  \App\User  $user
     * @param  \App\Faculty $faculty
     * @return mixed
     */
    public function delete(User $user, Faculty $faculty)
    {
        return strtolower($user->role) == 'admin';
    }

    /**
     * Determine whether the user can restore the university.
     *
     * @param  \App\User  $user
     * @param  \App\Faculty $faculty
     * @return mixed
     */
    public function restore(User $user, Faculty $faculty)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the university.
     *
     * @param  \App\User  $user
     * @param  \App\Faculty $faculty
     * @return mixed
     */
    public function forceDelete(User $user, Faculty $faculty)
    {
        return strtolower($user->role) == 'admin';
    }
}
