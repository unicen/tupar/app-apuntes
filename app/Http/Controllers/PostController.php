<?php

namespace App\Http\Controllers;

use App\Media;
use App\Post;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    public function __construct()
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user')
            ->with('subject')
            ->orderBy('created_at', 'desc')
            ->paginate(15);
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::select('id','name')->get();
        return view('posts.create', [
            'subjects' => $subjects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'content'=>'required',
            'subject_id' =>'required',
        ]);

        $post = new Post([
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'user_id' => Auth::user()->id,
            'subject_id' => $request->get('subject_id'),
        ]);

        DB::transaction(function () use ($request, $post){
            $post->save();
            $this->uploadImages($request, $post);
        });

        return redirect('/posts')->with('success', '¡Mensaje creado con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post->views()->create();
        return view('posts.show', [
            'post' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $this->authorize('update', $post);

        $subjects = Subject::select('id','name')->get();
        return view('posts.edit', [
            'subjects' => $subjects,
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->authorize('update', $post);

        $request->validate([
            'title'=>'required',
            'content'=>'required',
            'subject_id'=>'required',
        ]);

        $post->title =  $request->get('title');
        $post->content = $request->get('content');
        $post->subject_id = $request->get('subject_id');
        $post->save();

        return redirect('/posts')->with('success', '¡Publicacion actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);


        DB::transaction(function() use ($post) {
            $mediaFiles = $post->media;
            foreach ($mediaFiles as $media)
            {
                Storage::delete($media->path);
                $media->delete();
            }

            $post->delete();
        });

        return redirect('/posts')->with('success', '¡Publicacion eliminada con éxito!');
    }

    /**
     * Store a newly created media resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadImages(Request $request, Post $post)
    {
        $request->validate([
            'filesToUpload.*'=>'mimes:jpeg,bmp,jpg,png,pdf,doc,docx,odt|between:1, 2000',
        ]);

        if(isset($request->filesToUpload) && is_array($request->filesToUpload)){
            foreach($request->filesToUpload as $file ){
                $ext = $file->getClientOriginalExtension();
                $filename = $post->id . '-' . uniqid() . '.' . $ext;
                Storage::putFileAs('public/post_files', $file, $filename);
                $path = 'public/post_files/' . $filename;
                $mime = Storage::mimeType($path) ? Storage::mimeType($path) : 'n/a';
                Media::create([
                    'path' => $path,
                    'mime_type' => $mime ,
                    'post_id' => $post->id,
                ]);
            }
        }

    }
}
