<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')
            ->setScopes(['read:user', 'user:email'])
            ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        $provider = 'github';
        $userSocial = Socialite::driver($provider)->stateless()->user();

        $users = User::where(['email' => $userSocial->getEmail()])->first();

        // Login user if its already connected with this provider or create de user.
        if($users){
            Auth::login($users);
            return redirect('/');
        }else{
            DB::transaction(function() use ($userSocial, $provider){
                $user = User::create([
                    'name'          => $userSocial->getName(),
                    'email'         => $userSocial->getEmail(),
                    'provider_id'   => $userSocial->getId(),
                    'provider'      => $provider,
                ]);
                $this->saveAvatar($user, $userSocial->getAvatar());
                Auth::login($user);
            }); // End Transaction

            return redirect()->route('home');
        }
    }

    private function saveAvatar($user, $avatarUrl)
    {
        $client = new Client();
        $storageFolder = 'public/avatars/';
        //if(!Storage::exists($storageFolder))

        Storage::makeDirectory($storageFolder);

        //mime_content_type()

        $outputPath = "{$storageFolder}{$user->id}";

        $client->request('GET', $avatarUrl, [
            'sink' => storage_path("app/{$outputPath}")
        ]);

        $fileMime = mime_content_type(storage_path("app/{$outputPath}"));

        $supportedMimes = collect([[
            "mime" => "image/jpeg",
            "ext" => "jpg"
            ]
        ]);

        $mimeType = $supportedMimes->filter(function($supportedMime) use ($fileMime){
            return $fileMime == $supportedMime['mime'];
        })->first();

        $ext = $mimeType["ext"];

        $storagePath = "$outputPath.$ext";
        Storage::move($outputPath, $storagePath);
        $user->image = $storagePath;
        $user->save();

    }
}





