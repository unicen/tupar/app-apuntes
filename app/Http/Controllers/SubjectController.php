<?php

namespace App\Http\Controllers;

use App\Subject;
use App\Faculty;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Subject::class);

        $subjects = Subject::all();
        return view('subjects.index', [
            'subjects' => $subjects
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Subject::class);

        $faculties = Faculty::select('id', 'name')->get();
        return view('subjects.create', [
            'faculties' => $faculties,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Faculty::class);

        //'name', 'ref_link', 'slug', 'faculty_id'

        $request->validate([
            'name'=>'required',
            'faculty_id'=>'required',
        ]);

        $subject = new Subject([
            'name' => $request->get('name'),
            'faculty_id' => $request->get('faculty_id'),
        ]);
        $subject->save();
        return redirect('/subjects')->with('success', 'Catedra cargada con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        $this->authorize('view', $subject);

        return view ('subjects.show', ['subject'=>$subject]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        $this->authorize('update', $subject);

        $faculties = Faculty::select('id', 'name')->get();
        return view('subjects.edit', [
            'faculties' => $faculties,
            'subject' => $subject
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $this->authorize('update', $subject);

         $request->validate([
            'name'=>'required',
            'faculty_id'=>'required'
        ]);

        $subject->name = $request->get('name');
        $subject->faculty_id = $request->get('faculty_id');
        $subject->slug = $request->has('slug') ? $request->get('slug') : null;

        $subject->save();
        return redirect('/subjects')->with('success', '¡Catedra actualizada con éxito!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        $subject->delete();
        return redirect('/subjects')->with('success', '¡Catedra eliminada con éxito!');

    }
}
