<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Media;
use App\Post;
use App\Services\EEImportService;
use App\Subject;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class EEController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return void
     */
    public function import(Request $request)
    {
        $eeImportService = new EEImportService('http://172.26.0.1:8000/api');

        $faculty = Faculty::first();

        $catedras = $this->syncCatedras($eeImportService, $faculty);

        $this->syncAportes($eeImportService, $catedras);

    }

    private function syncCatedras($eeImportService, $faculty)
    {
        // Catedras
        $response = $eeImportService->get('catedras');
        $catedras = $response->json();

        foreach(collect($catedras) as $catedra)
        {
            $previous = Subject::where('slug', $catedra['url_title'])->first();
            $exists = isset($previous) ? $previous->exists() : false;
            if(!$exists)
            {
                Subject::create([
                    'name' => $catedra['title'],
                    'slug' => $catedra['url_title'],
                    'ref_link' => $catedra['ref_link'],
                    'rss' => $catedra['rss'],
                    'faculty_id' => $faculty->id
                ]);
            }
        }

        $totalCatedras = sizeof($catedras);
        echo "Total de catedras: {$totalCatedras}";
        return collect($catedras);
    }

    private function syncAportes($eeImportService, $catedras)
    {
        // Catedras
        $response = $eeImportService->get('aportes');
        $aportes = $response->json();

        $aportes = collect($aportes)->map(function($aporte) {
            $aporte['path'] = str_replace("http://www.comoustedyasabe.com.ar","http://172.26.0.1:8000",$aporte['path']);
            return $aporte;
        });

        foreach(collect($aportes) as $aporte)
        {
            $previous = Post::where('title', $aporte['title'])->first();
            $exists = isset($previous) ? $previous->exists() : false;
            if(!$exists)
            {
                $catedra = $catedras->where('id',$aporte['catedra'])->first();
                $subject = Subject::where('slug', $catedra['url_title'])->first();

                $newPost = [
                    'title' => $aporte['title'],
                    'subject_id' => $subject->id,
                    'content' => '',
                    'slug' => Str::slug($aporte['title']),
                    'user_id' => 1,
                ];

                $post = Post::create($newPost);
                $this->syncFile($post, $aporte['path']);

            }
        }

        $totalAportes = sizeof($aportes);
        echo "Total de aportes: {$totalAportes}";
    }

    public function syncFile(Post $post, $url)
    {
        $path      = parse_url($url, PHP_URL_PATH);       // get path from url
        $extension = pathinfo($path, PATHINFO_EXTENSION); // get ext from path
        $filename  = pathinfo($path, PATHINFO_FILENAME);

        $path = 'public/post_files/' . $post->id . '-' . uniqid() . '.' . $extension;

        $client = new Client();
        $client->request('GET', $url, [
            'sink' => storage_path("app/{$path}")
        ]);


        $mime = Storage::mimeType($path) ? Storage::mimeType($path) : 'n/a';

        Media::create([
            'path' => $path,
            'mime_type' => $mime ,
            'post_id' => $post->id,
        ]);
    }


}
