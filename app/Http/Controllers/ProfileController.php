<?php

namespace App\Http\Controllers;

use App\Subject;
use App\User;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //

    public function show()
    {
        $posts = Post::where('user_id',Auth::id())->get();
        return view('profile.show', [
            'posts' => $posts
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $request->validate([
            'name'=> 'required',
            'profilepic'=>'mimes:jpeg,bmp,jpg,png|between:1, 2000',
        ]);

        $user = Auth::user();
        $user->name =  $request->get('name');
        $user->lastname = $request->has('lastname') && !empty($request->get('lastname')) ? $request->get('lastname') : '';

        // TODO: $user->profilepic = $request->get('subject_id');

        $user->save();

        return redirect('/profile')->with('success', '¡Perfil actualizado!');
    }
}
