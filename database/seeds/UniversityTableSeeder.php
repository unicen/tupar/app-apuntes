<?php

use Illuminate\Database\Seeder;

class UniversityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\University::create([
            'name' => 'Universidad Nacional del Centro de la Pcia. de Bs. As.',
            'link' => 'http://www.unicen.edu.ar',            
        ]);
    }
}
