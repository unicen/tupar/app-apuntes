<?php

use Illuminate\Database\Seeder;

class FacultyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $university = \App\University::first();
        \App\Faculty::create([
            'name' => 'Facultad de Cs. Exactas',            
            'university_id' => $university->id,
        ]);
    }
}
