<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'admin',
            'password' => bcrypt('12345678'),
            'email' => 'admin@admin.com',
            'role' => 'admin'
        ]);

        App\User::create([
            'name' => 'user',
            'password' => bcrypt('12345678'),
            'email' => 'user@user.com',
            'role' => 'user'
        ]);
    }
}
