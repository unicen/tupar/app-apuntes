@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 order-md-1 mb-4">
                <div class="container">
                    <div class="row justify-content-around">
                        <div class="col-md-6 mb-4">
                            <img class="img-fluid img-thumbnail" src="{{ Storage::url(Auth::user()->image) ?? '' }}" alt="">
                        </div>
                    </div>
                </div>


                <h4 class="d-flex justify-content-between align-items-center mb-3">

                    <span class="text-muted">{{ Auth::user()->name }}</span>
                </h4>
                <div class="list-group mb-3">
                    <a href="{{ route('profile.show') }}" class="list-group-item list-group-item-action active">
                        Cuenta
                    </a>
                    <div class="d-none">
                        <a href="#" class="list-group-item list-group-item-action">Mis publicaciones</a>
                        <a href="#" class="list-group-item list-group-item-action">Ayuda</a>
                    </div>

                </div>

            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Información personal</h4>
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <form method="post" action="{{ route('profile.update') }}" class="needs-validation mb-4" >
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <br />
                    @endif
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="name">Nombre <span class="text-muted">Obligatorio</span></label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ Auth::user()->name }}" required="">

                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastame">Apellido</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="" value="{{ Auth::user()->lastname }}">
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" disabled value="{{ Auth::user()->email }}">
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Actualizar datos</button>
                </form>
                <h4 class="mb-4">Integraciones</h4>
                <div>
                    <div class="row">
                        <div class="col-md-6">
                            <p><i class="fab fa-github"></i> Github</p>
                        </div>
                        <div class="col-md-6">
                            @if( Auth::user()->provider == 'github')
                                <button class="btn btn-block btn-secondary" disabled>Conectado</button>
                            @else
                                <a href="{{ url('/login/github') }}" class="btn btn-block btn-primary">Conectar</a>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection
