@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center mb-4">

        <div class="col-md-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="card mb-4">
              <div class="card-body">
                  <h5 class="card-title">Publicar nuevo material</h5>
                  <p class="card-text">¿Tenés material para compartir? Compartilo acá</p>
                  <a href="{{ route('posts.create') }}" class="btn btn-primary">Nueva publicación</a>
              </div>
            </div>

            <div class="row justify-content-around m-6">
                <div class="col-md-8">
                    <form method="GET" action="{{ route('posts.index') }}" >
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Ingresa tu búsqueda" aria-label="Ingresa tu búsqueda" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="fas fa-search"></i> Buscar</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

            <h5 class="mb-4">Publicaciones más recientes</h5>

            @if(sizeof($posts) == 0)
                <div class="alert alert-warning" role="alert">
                    Ups! Lo sentimos, no hemos encontrado publicaciones.
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <ul class="list-unstyled">
                        @foreach ($posts as $post)
                            <li class="media">
                            <img width="50" src="{{ Storage::url($post->user->image) }}" class="mr-3" alt="{{ $post->user->name }}">
                            <div class="media-body">
                                <a href="{{ route('posts.show',$post->slug) }}"><h5 class="card-title">{{ $post->title }}</h5></a>
                                <p class="card-text">
                                    <small class="text-muted">Creado {{ $post->created_at->ago() }} por
                                        <a href="#">{{ $post->user->name }}</a>
                                        en {{ $post->subject->name }}   </small></p>
                                <p class="card-text">{{ Str::limit($post->content,'50', '...') }}</p>
                                <p>
                                    @if($post->media->count() > 0)
                                        <i class="fas fa-paperclip"></i> {{ $post->media->count() }} adjunto{{ $post->media->count() >= 2 ? 's' : '' }}
                                    @endif
                                </p>
                            </div>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>

@endsection


