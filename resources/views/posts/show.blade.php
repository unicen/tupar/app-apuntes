@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h1>{{ $post->title }}</h1>
                        <p>Publicado por <strong>{{$post->user->name}}</strong>. Publicado en: {{ $post->subject->name }}</p>
                        <p>{{ $post->content }}</p>
                        @foreach($post->media as $media)
                            @if(strpos($media->mime_type,'image') !== false)
                                <div class="text-center">
                                    <img class="img-thumbnail vh-100" src="{{ Storage::url($media->path) }}" alt="{{$post->title}}">
                                </div>
                            @elseif ($media->mime_type === 'video/mp4')
                                <video width="640" height="480" controls>
                                    <source src="{{ Storage::url($media->path) }}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            @elseif ($media->mime_type === 'audio/mpeg')
                                <audio class="col-md-10 offset-1" controls>
                                    <source src="{{ Storage::url($media->path) }}" type="audio/mpeg">
                                    Your browser does not support the audio element.
                                </audio>
                            @elseif ($media->mime_type === 'application/pdf')
                                <div class="">
                                    <iframe class="w-100 vh-100" src = "/ViewerJS/#../{{ Storage::url($media->path) }}" allowfullscreen webkitallowfullscreen></iframe>
                                </div>
                            @else
                                <a href="{{ Storage::url($media->path) }}" target="_blank">Descargar</a>
                            @endif
                        @endforeach

                        @canany(['update','delete'], $post)
                            <hr>
                            <div class="d-inline-flex">
                                @can('update',$post)
                                    <a href="{{ route('posts.edit',$post->id) }}" class="btn btn-primary mr-2">Editar</a>
                                @endcan
                                @can('delete',$post)
                                    <form action="{{ route('posts.destroy', $post->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Borrar</button>
                                    </form>
                                @endcan
                            </div>
                        @endcanany
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
