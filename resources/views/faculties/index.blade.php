@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Facultades</h1>
            <table class="table table-striped">
                <thead>
                <a href=" {{ route('faculties.create') }}" class="btn btn-primary" >Agregar</a>
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <tr>
                    <td>Nombre</td>
                    <td>Universidad</td>
                    <td>Opciones</td>
                </tr>
                </thead>
                <tbody>
                @foreach($faculties as $faculty)
                    <tr>
                        <td><a href=" {{ route('faculties.show', $faculty->id) }}"> {{ $faculty->name }} </a></td>
                        <td>{{ $faculty->university()->first()->name }}</td>
                        <td class="d-inline-flex">
                                @can('update',$faculty)
                                    <a href="{{ route('faculties.edit',$faculty->id)}}" class="btn btn-primary mr-1">Editar</a>
                                @endcan
                                @can('delete',$faculty)
                                    <form action="{{ route('faculties.destroy', $faculty->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Borrar</button>
                                    </form>
                                @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
