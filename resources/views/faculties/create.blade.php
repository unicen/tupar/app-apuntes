@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Agregar Facultad</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('faculties.store') }}">
          @csrf
          <div class="form-group">    
              <label for="name">Nombre:</label>
              <input type="text" class="form-control" name="name"/>
          </div>

          <div class="form-group">
              <label for="university_id">Universidad:</label>
              <select type="text" class="form-control" name="university_id" id="">
                <option disabled selected>Seleccione una universidad...</option>
                @foreach($universities as $university)
                  <option value="{{ $university->id }}">{{ $university->name }}</option>
                @endforeach
              </select>
          </div>
                         
          <button type="submit" class="btn btn-primary-outline">Agregar facultad</button>
      </form>
  </div>
</div>
</div>
@endsection
