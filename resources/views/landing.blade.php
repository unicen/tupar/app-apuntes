<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Grupo CUYS">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
    <style>
        :root {
            --jumbotron-padding-y: 3rem;
        }

        .jumbotron {
            padding-top: var(--jumbotron-padding-y);
            padding-bottom: var(--jumbotron-padding-y);
            margin-bottom: 0;
            background-color: #fff;
        }
        @media (min-width: 768px) {
            .jumbotron {
                padding-top: calc(var(--jumbotron-padding-y) * 2);
                padding-bottom: calc(var(--jumbotron-padding-y) * 2);
            }
        }

        .jumbotron p:last-child {
            margin-bottom: 0;
        }

        .jumbotron-heading {
            font-weight: 300;
        }

        .jumbotron .container {
            max-width: 40rem;
        }

        footer {
            padding-top: 3rem;
            padding-bottom: 3rem;
        }

        footer p {
            margin-bottom: .25rem;
        }

        .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }


    </style>

</head>

<body>
<div id="app">

    <header>
        <nav class="navbar navbar-expand-xl navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'CUYS') }}</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample06" aria-controls="navbarsExample06" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExample06">
                    @if (Route::has('login'))
                        <ul class="navbar-nav ml-auto">
                            @auth
                                <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Home</a></li>
                            @else
                                <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>

                                @if (Route::has('register'))
                                    <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                                @endif
                            @endauth
                        </ul>
                    @endif
                </div>
            </div>
        </nav>
    </header>

    <main role="main">

        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">CUYS</h1>
                <p class="lead text-muted">CUYS es una plataforma open source de publicación de contenidos.</p>
                <p>
                    <a href="{{ route('register') }}" class="btn btn-primary my-2">¡Quiero registrarme!</a>
                    <a href="{{ route('home') }}" class="btn btn-secondary my-2">Navegar como invitado</a>
                </p>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-4">
                            <div class="row p-4">
                                <div class="col-md-4 p-4">
                                    <img class="card-img-top" src="{{ url('svg/mobile_devices.svg') }}" alt="Card image cap">
                                </div>
                                <div class="col-md-8 flex align-self-center">
                                    <p class="lead card-text">Nos esforzamos en una vista práctica y ajustable a todos los dispositivos para una mejor visualización de todos los documentos.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mb-4">
                            <div class="row p-4">
                                <div class="col-md-8 flex align-self-center">
                                    <p class="lead card-text">Hacé tus propios aportes y ayuda a creccer a esta comunidad.</p>
                                    <p class="lead card-text">Soportamos todo tipo de formato de archivos para tus publicaciones: texto, audio, imágenes, PDF, etc.</p>
                                </div>
                                <div class="col-md-4 p-4">
                                    <img class="card-img-top" src="{{ url('svg/publish_article.svg') }}" alt="Card image cap">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Volver al inicio</a>
            </p>
            <p>CUYS se encuentra registrado bajo la <a href="https://opensource.org/licenses/MIT" target="_blank">Licencia MIT</a>.</p>
            <p>Sos nuevo en CUYS? <a href="{{ route('home') }}">Visita nuestra página</a> o lee nuestra <a href="../../getting-started/">Guía para comenzar</a>.</p>
        </div>
    </footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="../../assets/js/vendor/holder.min.js"></script>
</body>
</html>
