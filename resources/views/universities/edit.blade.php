@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar universidad</div>

                <div class="card-body">
                  <form class="" method="post" action="{{ route('universities.update', $university->id) }}">
                  @method('PATCH')
                  @csrf
                  @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  <br />
                  @endif

                  <div class="form-group">
                    <label for="university">Universidad</label>
                    <input type="text" class="form-control" name="name" value="{{ $university->name }}" />
                  </div>

                  <div class="form-group">
                    <label for="link">Link:</label>
                    <input class="text" name="link"  value ="{{ $university->link }}" />
                  </div>

                  <button type="submit" class="btn btn-primary">Actualizar universidad</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
