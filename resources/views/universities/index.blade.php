@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="display-3">Universidades</h1>
                <table class="table table-striped">
                    <thead>
                    <a href=" {{ route('universities.create') }}" class="btn btn-primary" >Agregar</a>
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    <tr>
                        <td>Nombre</td>
                        <td>Link</td>
                        <td>Opciones</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($universities as $university)
                        <tr>
                            <td><a href=" {{ route('universities.show', $university->id) }}"> {{ $university->name }} </a></td>
                            <td>{{$university->link}}</td>
                            <td class="d-inline-flex">
                                @can('update',$university)
                                    <a href="{{ route('universities.edit',$university->id)}}" class="btn btn-primary mr-1">Editar</a>
                                @endcan

                                @can('delete',$university)
                                    <form action="{{ route('universities.destroy', $university->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Borrar</button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
