@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-md-12">
            <div class="alert alert-warning" role="alert">
                <i class="fas fa-exclamation-triangle mr-1"></i> El sitio está en mantenimiento y algunas funciones pueden no estar disponibles, disculpe las molestias.
            </div>

            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <h3>¡Bienvenido {{ Auth::user()->name ?? 'invitado' }}!</h3>
                        <p>Te recordamos que <strong>CUYS</strong> es mantenido por alumnos de la Universidad Nacional del Centro de la Provincia de Buenos Aires.</p>
                        <p><i class="fas fa-bolt mr-1" style="color: rgb(253, 126, 20);"></i> ¡Estamos en constante evolución! Contribuí en nuestro repositorio oficial en <a target="_blank" href="https://gitlab.com/unicen/tupar/app-apuntes">Gitlab</a>.</p>


                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mb-4">
        <div class="col-md-12">
            <div class="row mb-4">
                <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Publicar nuevo mensaje!</h5>
                        <p class="card-text">¿Tenés material para compartir? Compartilo acá</p>
                        <a href="{{ route('posts.create') }}" class="btn btn-primary">Nueva publicación</a>
                    </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title mb-4"><i class="far fa-heart mr-1"></i> Más leídos</h5>

                            @foreach($mostViewed as $post)
                                <div class="media">
                                    <div class="media-body">
                                        <p class=""><a href="{{ route('posts.show', $post->slug) }}">{{ $post->title }}</a> en {{ $post->subject->name }}</p>

                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3 class="mb-4">Publicaciones recientes <small class="ml-4"><a href="{{ route('posts.index') }}">Ver todas</a></small></h3>


    <div class="card col-md-12 mb-4" >
        <div class="card-body">

            <ul class="list-unstyled">
                @foreach ($latestPosts as $post)
                    <li class="media">
                        <img width="50" src="{{ Storage::url($post->user->image) }}" class="mr-3" alt="{{ $post->user->name }}">
                        <div class="media-body">
                            <a href="{{ route('posts.show',$post->slug) }}"><h5 class="card-title">{{ $post->title }}</h5></a>
                            <p class="card-text">
                                <small class="text-muted">Creado {{ $post->created_at->ago() }} por
                                    <a href="#">{{ $post->user->name }}</a>
                                    en {{ $post->subject->name }}   </small></p>
                            <p class="card-text">{{ Str::limit($post->content,'50', '...') }}</p>
                            <p>
                                @if($post->media->count() > 0)
                                    <i class="fas fa-paperclip"></i> {{ $post->media->count() }} adjunto{{ $post->media->count() >= 2 ? 's' : '' }}
                                @endif
                            </p>
                        </div>
                    </li>
                @endforeach
            </ul>



        </div>
    </div>

</div>


@endsection
