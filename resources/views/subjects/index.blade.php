@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Cátedras</h1>
            <table class="table table-striped">
                <thead>
                <a href=" {{ route('subjects.create') }}" class="btn btn-primary" >Agregar</a>
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <tr>
                    <td>Nombre</td>
                    <td>Facultad</td>
                    <td>Opciones</td>
                </tr>
                </thead>
                <tbody>
                @foreach($subjects as $subject)
                    <tr>
                        <td><a href=" {{ route('subjects.show', $subject->slug) }}"> {{ $subject->name }} </a></td>
                        <td>{{ $subject->faculty()->first()->name }}</td>
                        <td class="d-inline-flex">
                            @can('update',$subject)
                                <a href="{{ route('subjects.edit',$subject->slug)}}" class="btn btn-primary mr-1">Editar</a>
                            @endcan
                            @can('delete',$subject)
                                <form action="{{ route('subjects.destroy', $subject->slug)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Eliminar</button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
