@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Agregar Cátedra</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('subjects.store') }}">
          @csrf
          <div class="form-group">
              <label for="name">Nombre:</label>
              <input type="text" class="form-control" name="name" required/>
          </div>

          <div class="form-group">
              <label for="university_id">Facultad:</label>
              <select type="text" class="form-control" name="faculty_id" id="">
                <option disabled selected>Seleccione una facultas...</option>
                @foreach($faculties as $faculty)
                  <option value="{{ $faculty->id }}">{{ $faculty->name }}</option>
                @endforeach
              </select>
          </div>

          <div class="form-group">
              <label for="name">Link:</label>
              <input type="text" class="form-control" name="ref_link"/>
          </div>

          <button type="submit" class="btn btn-primary-outline">Agregar Cátedra</button>
      </form>
  </div>
</div>
</div>
@endsection
