@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Cátedra</div>

                <div class="card-body">
                  <form class="" method="post" action="{{ route('subjects.update', $subject->slug) }}">
                  @method('PATCH')
                  @csrf
                  @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  <br />
                  @endif

                  <div class="form-group">
                    <label for="faculty">Cátedra</label>
                    <input type="text" class="form-control" name="name" value="{{ $subject->name }}"/>
                  </div>

                  <div class="form-group">
                      <label for="slug">Slug</label>
                      <input type="text" class="form-control" name="slug" value="{{ $subject->slug }}"/>
                  </div>

                  <div class="form-group">
                    <label for="university_id">Facultad:</label>

                    <select type="text" class="form-control" name="faculty_id" id="">
                      <option disabled>Seleccione una facultad...</option>
                      @foreach($faculties as $faculty)
                        <option value="{{ $faculty->id }}" {{ $faculty->id == $subject->faculty_id ? 'selected' : '' }}>{{ $faculty->name }}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                      <label for="ref_link">Link</label>
                      <input type="text" class="form-control" name="ref_link" value="{{ $subject->ref_link }}"/>
                  </div>

                  <button type="submit" class="btn btn-primary">Actualizar cátedra</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
